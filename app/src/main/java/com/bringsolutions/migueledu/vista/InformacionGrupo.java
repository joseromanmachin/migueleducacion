package com.bringsolutions.migueledu.vista;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.migueledu.R;
import com.bringsolutions.migueledu.controlador.AdaptadorAlumnos;
import com.bringsolutions.migueledu.controlador.AdaptadorCursos;
import com.bringsolutions.migueledu.modelo.Alumno;
import com.bringsolutions.migueledu.modelo.CONSTANTES;
import com.bringsolutions.migueledu.modelo.Curso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InformacionGrupo extends AppCompatActivity {
	Intent intent;
	String idGrupo_Recibido, nombreGrupo_Recibido, descripcionGrupo_Recibido;
	TextView nombreGrupo, descripcionGrupo;
	
	RecyclerView recyclerView;
	AdaptadorAlumnos adapter;
	List<Alumno> ALUMNOS = new ArrayList<>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_informacion_grupo);
		inicializarComponentes();
		intent= getIntent();
		
		idGrupo_Recibido = intent.getStringExtra("idGrupito");
		nombreGrupo_Recibido=intent.getStringExtra("nombreGrupito");
		descripcionGrupo_Recibido=intent.getStringExtra("descripcionGrupito");
		nombreGrupo.setText(nombreGrupo_Recibido);
		descripcionGrupo.setText(descripcionGrupo_Recibido);
		
		recyclerView =  findViewById(R.id.recycler_Alumnos);
		recyclerView.setLayoutManager(new LinearLayoutManager(InformacionGrupo.this));
	
		llenarRecyclerAlumnos(idGrupo_Recibido);
		
	}
	
	public void llenarRecyclerAlumnos(String idGrupo_recibido) {
		
		String url= CONSTANTES.linkBaseApi+"consultarAlumnosGrupos&grupo="+idGrupo_recibido;
		//Crear un objeto requetqueue
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		
		//Url del webservice
		// crear un objeto requet
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				
				JSONArray json = response.optJSONArray("educacion");
				
				JSONObject jsonObject = null;
				
				try {
					
					for (int i =0 ; i<json.length();i++){
						jsonObject = json.getJSONObject(i);
						String idA=jsonObject.optString("usu_id");
						String nombreA   =  jsonObject.optString("usu_nombre");
						String apPaternoA   =  jsonObject.optString("usu_apellido_paterno");
						String apMaternoA   =  jsonObject.optString("usu_apellido_materno");
						String emailA =  jsonObject.optString("usu_email");
						String telefonoA = jsonObject.optString("usu_telefono");
						String fkGrupoA=jsonObject.optString("fk_grupo");
						ALUMNOS.add(new Alumno(idA, nombreA, apPaternoA, apMaternoA,emailA,telefonoA,fkGrupoA));
						
					}
					
					adapter = new AdaptadorAlumnos(ALUMNOS,InformacionGrupo.this);
					recyclerView.setAdapter(adapter);
					
					
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(), "Hay un problema de excepción: " + e, Toast.LENGTH_SHORT).show();
					
				}
			}
		}, new Response.ErrorListener() {
			@Override
			
			
			public void onErrorResponse(VolleyError error) {
				
				Toast.makeText(getApplicationContext(), "Hay un problema de volley", Toast.LENGTH_SHORT).show();
				
			}
		});
		
		//añadir el objeto requet al requetqueue
		requestQueue.add(jsonObjectRequest);
		
		
		
		
	}
	
	
	public void inicializarComponentes() {
		nombreGrupo = findViewById(R.id.banner_NombreGrupo);
		descripcionGrupo = findViewById(R.id.banner_DescripcionGrupo);
		
	}
}
