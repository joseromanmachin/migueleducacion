package com.bringsolutions.migueledu.vista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bringsolutions.migueledu.MainActivity;
import com.bringsolutions.migueledu.R;
import com.bringsolutions.migueledu.controlador.Acciones;

public class Login extends AppCompatActivity {
	Acciones acciones;
	
	TextView textoButtonRegistrar;
	Button btnIniciarSesion;
	EditText txt_TelefonoIniciarSesion;
	EditText txt_PassIniciarSession;
	
	String telefono, password;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		acciones= new Acciones(this);
		textoButtonRegistrar = findViewById(R.id.textoRegistrarse);
		btnIniciarSesion= findViewById(R.id.btnIniciarSesion);
		txt_TelefonoIniciarSesion = findViewById(R.id.txtTelefono);
		txt_PassIniciarSession=findViewById(R.id.txtPass);
		txt_TelefonoIniciarSesion.setText("9933951421");
		txt_PassIniciarSession.setText("100990201-4");
		
		acciones();
		
	}
	
	
	
	void acciones(){
		
		
		textoButtonRegistrar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				acciones.irOtraActivity(Login.this, new Registro());
			}
		});
		
		
		btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				telefono = txt_TelefonoIniciarSesion.getText().toString();
				password=txt_PassIniciarSession.getText().toString();
				acciones.iniciarSesion(telefono,password, Login.this, new Principal());
				finish();
				
			}
		});
	}
	
	
	
}
