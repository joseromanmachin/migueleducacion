package com.bringsolutions.migueledu.vista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bringsolutions.migueledu.MainActivity;
import com.bringsolutions.migueledu.R;
import com.bringsolutions.migueledu.controlador.Acciones;

public class Registro extends AppCompatActivity {
	Acciones     acciones;
	EditText     txt_Registro_NombreUsuario;
	EditText     txt_Registro_ApellidoPaternoUsuario;
	EditText     txt_Registro_ApellidoMaternoUsuario;
	EditText     txt_Registro_EmailUsuario;
	EditText     txt_Registro_TelefonoUsuario;
	EditText     txt_Registro_Password1Usuario;
	EditText     txt_Registro_Password2Usuario;
	RadioButton  registro_RadioTipoAlumno;
	RadioButton  registro_RadioTipoProfesor;
	Button       registro_Boton;
	
	String      registro_NombreUsuario;
	String      registro_ApellidoPaternoUsuario;
	String      registro_ApellidoMaternoUsuario;
	String      registro_EmailUsuario;
	String      registro_TelefonoUsuario;
	String      registro_Password1Usuario;
	String      registro_Password2Usuario;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		inicializarElementos();
		accionesClase();
	}
	
	public void inicializarElementos(){
		txt_Registro_NombreUsuario= findViewById(R.id.txtNombreUsuario);
		txt_Registro_ApellidoPaternoUsuario= findViewById(R.id.txtApellidoPaterno);
		txt_Registro_ApellidoMaternoUsuario= findViewById(R.id.txtApellidoMaterno);
		txt_Registro_EmailUsuario= findViewById(R.id.txtEmail);
		txt_Registro_TelefonoUsuario= findViewById(R.id.txtTelefonoUsuario);
		txt_Registro_Password1Usuario= findViewById(R.id.pass);
		txt_Registro_Password2Usuario= findViewById(R.id.pass2);
		registro_RadioTipoAlumno= findViewById(R.id.estudianteRadio);
		registro_RadioTipoProfesor= findViewById(R.id.profesorRadio);
		registro_Boton=findViewById(R.id.btnRegistrarse);
		
		acciones=new Acciones(this);
	}
	
	public void accionesClase(){
		        registro_Boton.setOnClickListener(new View.OnClickListener() {
			    @Override
			     public void onClick(View v) {
				registro_NombreUsuario=txt_Registro_NombreUsuario.getText().toString();
				registro_ApellidoPaternoUsuario=txt_Registro_ApellidoPaternoUsuario.getText().toString();
				registro_ApellidoMaternoUsuario=txt_Registro_ApellidoMaternoUsuario.getText().toString();
				registro_EmailUsuario=txt_Registro_EmailUsuario.getText().toString();
				registro_TelefonoUsuario=txt_Registro_TelefonoUsuario.getText().toString();
				registro_Password1Usuario=txt_Registro_Password1Usuario.getText().toString();
				registro_Password2Usuario=txt_Registro_Password2Usuario.getText().toString();
				String tipoUsuario = acciones.verificarTipoUsuarioSeleccionadoRegistro(registro_RadioTipoAlumno, registro_RadioTipoProfesor);
				
				if(acciones.comprobarPasswords(registro_Password1Usuario, registro_Password2Usuario)==true){
					
					acciones.registrarUsuario(registro_NombreUsuario,registro_ApellidoPaternoUsuario, registro_ApellidoMaternoUsuario, registro_EmailUsuario, registro_TelefonoUsuario, tipoUsuario, registro_Password1Usuario, Registro.this, new Login());
					
					
				}else{
						Toast.makeText(getApplicationContext(),"LAS CONTRASEÑAS SON CORTAS O NO SON IGUALES", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return false;
	}
}
