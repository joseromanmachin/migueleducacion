package com.bringsolutions.migueledu.vista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.migueledu.R;
import com.bringsolutions.migueledu.controlador.AdaptadorCursos;
import com.bringsolutions.migueledu.controlador.AdaptadorGrupos;
import com.bringsolutions.migueledu.modelo.CONSTANTES;
import com.bringsolutions.migueledu.modelo.Curso;
import com.bringsolutions.migueledu.modelo.Grupo;
import com.bringsolutions.migueledu.modelo.Usuario;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bringsolutions.migueledu.modelo.CONSTANTES.USUARIO;

public class Principal extends AppCompatActivity {
	RecyclerView recyclerView;
	AdaptadorCursos adapter;
	List<Curso> CURSOS = new ArrayList<>();
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);
		
		recyclerView =  findViewById(R.id.recyclerCursos);
		recyclerView.setLayoutManager(new LinearLayoutManager(Principal.this));
		
		llenarRecyclerCursos(USUARIO.getId());
		
	}
	
	
	
	public void llenarRecyclerCursos( String id_user) {
		
		String url= CONSTANTES.linkBaseApi+"consultarCursos&usuario="+id_user;
		//Crear un objeto requetqueue
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		
		//Url del webservice
		// crear un objeto requet
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				
				JSONArray json = response.optJSONArray("educacion");
				
				JSONObject jsonObject = null;
				
				try {
					
					for (int i =0 ; i<json.length();i++){
						jsonObject = json.getJSONObject(i);
						String id_Curso   =  jsonObject.optString("cur_id");
						String titulo_Curso   =  jsonObject.optString("cur_nombre");
						String descripcion_Curso   =  jsonObject.optString("cur_descripcion");
						String id_Usuario_Curso =  jsonObject.optString("usu_id");
						CURSOS.add(new Curso(id_Curso, titulo_Curso, descripcion_Curso, id_Usuario_Curso));
						
					}
					
					adapter = new AdaptadorCursos(CURSOS,Principal.this);
					recyclerView.setAdapter(adapter);
					
					
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(), "Hay un problema de excepción: " + e, Toast.LENGTH_SHORT).show();
					
				}
			}
		}, new Response.ErrorListener() {
			@Override
			
			
			public void onErrorResponse(VolleyError error) {
				
				Toast.makeText(getApplicationContext(), "Hay un problema de volley", Toast.LENGTH_SHORT).show();
				
			}
		});
		
		//añadir el objeto requet al requetqueue
		requestQueue.add(jsonObjectRequest);
		
		
		
		
		
		
	}
}
