package com.bringsolutions.migueledu.modelo;

public class Usuario {
	
	String id;
	String nombre;
	String telefono;
	String tipo;
	String pass;
	
	
	public Usuario(){}
	
	public Usuario(String id, String nombre, String telefono, String tipo, String pass) {
		this.id = id;
		this.nombre = nombre;
		this.telefono = telefono;
		this.tipo = tipo;
		this.pass = pass;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}
}
