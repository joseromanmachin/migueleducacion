package com.bringsolutions.migueledu.modelo;

public class Alumno {
	
	String id_Alumno;
	String nombre_Alumno;
	String apPaterno_Alumno;
	String apMaterno_Alumno;
	String email_Alumno;
	String telefono_Alumno;
	String idFK_Grupo_Alumno;
	
	
	public Alumno(){
	}
	
	public Alumno(String id_Alumno, String nombre_Alumno, String apPaterno_Alumno, String apMaterno_Alumno, String email_Alumno, String telefono_Alumno, String idFK_Grupo_Alumno) {
		this.id_Alumno = id_Alumno;
		this.nombre_Alumno = nombre_Alumno;
		this.apPaterno_Alumno = apPaterno_Alumno;
		this.apMaterno_Alumno = apMaterno_Alumno;
		this.email_Alumno = email_Alumno;
		this.telefono_Alumno = telefono_Alumno;
		this.idFK_Grupo_Alumno = idFK_Grupo_Alumno;
	}
	
	public String getId_Alumno() {
		return id_Alumno;
	}
	
	public void setId_Alumno(String id_Alumno) {
		this.id_Alumno = id_Alumno;
	}
	
	public String getNombre_Alumno() {
		return nombre_Alumno;
	}
	
	public void setNombre_Alumno(String nombre_Alumno) {
		this.nombre_Alumno = nombre_Alumno;
	}
	
	public String getApPaterno_Alumno() {
		return apPaterno_Alumno;
	}
	
	public void setApPaterno_Alumno(String apPaterno_Alumno) {
		this.apPaterno_Alumno = apPaterno_Alumno;
	}
	
	public String getApMaterno_Alumno() {
		return apMaterno_Alumno;
	}
	
	public void setApMaterno_Alumno(String apMaterno_Alumno) {
		this.apMaterno_Alumno = apMaterno_Alumno;
	}
	
	public String getEmail_Alumno() {
		return email_Alumno;
	}
	
	public void setEmail_Alumno(String email_Alumno) {
		this.email_Alumno = email_Alumno;
	}
	
	public String getTelefono_Alumno() {
		return telefono_Alumno;
	}
	
	public void setTelefono_Alumno(String telefono_Alumno) {
		this.telefono_Alumno = telefono_Alumno;
	}
	
	public String getIdFK_Grupo_Alumno() {
		return idFK_Grupo_Alumno;
	}
	
	public void setIdFK_Grupo_Alumno(String idFK_Grupo_Alumno) {
		this.idFK_Grupo_Alumno = idFK_Grupo_Alumno;
	}
}
