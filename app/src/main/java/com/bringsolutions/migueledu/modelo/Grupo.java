package com.bringsolutions.migueledu.modelo;

public class Grupo {
	
	String id_grupo;
	String nombre_grupo;
	String curso_id_curso;
	String grupo_descripcion;
	
	public Grupo(String id_grupo, String nombre_grupo, String curso_id_curso, String grupo_descripcion) {
		this.id_grupo = id_grupo;
		this.nombre_grupo = nombre_grupo;
		this.curso_id_curso = curso_id_curso;
		this.grupo_descripcion = grupo_descripcion;
	}
	
	public String getId_grupo() {
		return id_grupo;
	}
	
	public void setId_grupo(String id_grupo) {
		this.id_grupo = id_grupo;
	}
	
	public String getNombre_grupo() {
		return nombre_grupo;
	}
	
	public void setNombre_grupo(String nombre_grupo) {
		this.nombre_grupo = nombre_grupo;
	}
	
	public String getCurso_id_curso() {
		return curso_id_curso;
	}
	
	public void setCurso_id_curso(String curso_id_curso) {
		this.curso_id_curso = curso_id_curso;
	}
	
	public String getGrupo_descripcion() {
		return grupo_descripcion;
	}
	
	public void setGrupo_descripcion(String grupo_descripcion) {
		this.grupo_descripcion = grupo_descripcion;
	}
}
