package com.bringsolutions.migueledu.modelo;



public class Curso {
	
	String id_curso;
	String nombre_curso;
	String descripcion_curso;
	String id_usuario_curso;
	
	
	public Curso(){
	}
	
	public Curso(String id_curso, String nombre_curso, String descripcion_curso, String id_usuario_curso) {
		this.id_curso = id_curso;
		this.nombre_curso = nombre_curso;
		this.descripcion_curso = descripcion_curso;
		this.id_usuario_curso = id_usuario_curso;
	}
	
	
	public String getId_curso() {
		return id_curso;
	}
	
	public void setId_curso(String id_curso) {
		this.id_curso = id_curso;
	}
	
	public String getNombre_curso() {
		return nombre_curso;
	}
	
	public void setNombre_curso(String nombre_curso) {
		this.nombre_curso = nombre_curso;
	}
	
	public String getDescripcion_curso() {
		return descripcion_curso;
	}
	
	public void setDescripcion_curso(String descripcion_curso) {
		this.descripcion_curso = descripcion_curso;
	}
	
	public String getId_usuario_curso() {
		return id_usuario_curso;
	}
	
	public void setId_usuario_curso(String id_usuario_curso) {
		this.id_usuario_curso = id_usuario_curso;
	}
}
