package com.bringsolutions.migueledu.controlador;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.migueledu.modelo.CONSTANTES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.bringsolutions.migueledu.modelo.CONSTANTES.USUARIO;

public  class  Acciones {
	Context context;
	boolean respuestaExistenciaUsuario;
	
	public Acciones(Context context){
		this.context=context.getApplicationContext();
	}
	
	public boolean comprobarPasswords(String pass1, String pass2){
		boolean respuesta;
		
		if((pass1.equals(pass2)) && pass1.length()>5){
			respuesta=true;
			
		}else{
			respuesta=false;
		}
		
		return respuesta;
		
	}
	
	public String verificarTipoUsuarioSeleccionadoRegistro(RadioButton alumno, RadioButton profesor){
		//1=alumno 2=profesor
		String respuesta="";
		if(alumno.isChecked()==true){
			respuesta="1";
		}else if(profesor.isChecked()==true){
			respuesta="2";
		}
		return  respuesta;
	}
	
	public void registrarUsuario(String nombreUsuario, String apPaterno, String apMaterno, String Email, String telefono, String tipoUsuario, String pass, final Activity A_uno, final Activity A_dos){
		if(verificarExistenciaUsuario(Email, telefono)==true){
			RequestQueue requestQueue = Volley.newRequestQueue(context);
			String url=CONSTANTES.linkBaseApi+"registrarUsuario&nombre="+nombreUsuario+"&apellidoP="+apPaterno+"&apellidoM="+apMaterno+"&email="+Email+"&telefono="+telefono+"&password="+pass+"&tipo="+tipoUsuario;
			StringRequest stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					irOtraActivity(A_uno, A_dos);
					Toast.makeText(context, "USUARIO REGISTRADO CORRECTAMENTE!", Toast.LENGTH_LONG).show();
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Toast.makeText(context, "Problemas con conexión a Internet", Toast.LENGTH_LONG).show();
				}
			});
			requestQueue.add(stringRequest);
		}
	}
	
	public void irOtraActivity(Activity primeraActivity, Activity segundaActivity) {
		primeraActivity.getApplication().getApplicationContext().startActivity(new Intent(primeraActivity.getApplicationContext(), segundaActivity.getClass()));
	}
	
	public void iniciarSesion(String telefono, String pass, final Activity A_uno, final Activity A_dos){
		RequestQueue requestQueue = Volley.newRequestQueue(context);
		String url=CONSTANTES.linkBaseApi+"Login&telefono="+telefono+"&password="+pass;
		 JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				JSONArray json = response.optJSONArray("educacion");
				JSONObject jsonObject = null;
				try {
					//CARGO UN OBJETO DE TIPO USUARIO Y SETEO LOS DATOS CONSUMIDOS POR EL WEBSERVICE
					jsonObject=json.getJSONObject(0);
					USUARIO.setId(jsonObject.optString("usu_id"));
					USUARIO.setNombre(jsonObject.optString("usu_nombre"));
					USUARIO.setTelefono(jsonObject.optString("usu_telefono"));
					USUARIO.setTipo(jsonObject.optString("uso_tipo"));
					USUARIO.setPass(jsonObject.optString("usu_password"));
					irOtraActivity(A_uno, A_dos);
					//MANDO UN MENSAJE DE BIENVENIDA SI SE ENCONTRÓ EL USUARIO CONCATENANDO SU NOMBRE DEL OBJETO
					Toast.makeText(context,"¡Hola "+USUARIO.getNombre()+"!", Toast.LENGTH_SHORT).show();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(context,"El usuario no existe o los datos no son correctos.", Toast.LENGTH_SHORT).show();
			}
		});
		requestQueue.add(jsonObjectRequest);
	}
	
	public boolean verificarExistenciaUsuario(String correo, String telefono){
		RequestQueue requestQueue = Volley.newRequestQueue(context);
		String url=CONSTANTES.linkBaseApi+"verificarUsuario&telefono="+telefono+"&email="+correo;
		
		StringRequest stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				respuestaExistenciaUsuario=false;
				Toast.makeText(context,"YA EXISTE UNA CUENTA ASOCIADA A ESE CORREO O NÚMERO PROPORCIONADO PROPORCIONADO", Toast.LENGTH_LONG).show();
				
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				respuestaExistenciaUsuario=true;
			}
		});
		requestQueue.add(stringRequest);
		return respuestaExistenciaUsuario;
	}
	
}
