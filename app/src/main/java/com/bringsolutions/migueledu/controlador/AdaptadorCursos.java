package com.bringsolutions.migueledu.controlador;

import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.migueledu.R;
import com.bringsolutions.migueledu.modelo.CONSTANTES;
import com.bringsolutions.migueledu.modelo.Curso;
import com.bringsolutions.migueledu.modelo.Grupo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdaptadorCursos extends RecyclerView.Adapter<HolderCurso> {
	
	 List<Curso> listCursos;
	 Context c;
	
	
	 RecyclerView recyclerViewGrupos;
	 TextView nombre_Curso_Info;
	 ImageButton cerrarDialogo;
	 AdaptadorGrupos adapterGrupos;
	 List<Grupo> GRUPOS = new ArrayList<>();
	
	public AdaptadorCursos(List<Curso> cursosLista, Activity context) {
		this.listCursos=cursosLista;
		this.c = context;
		
	}
	
	
	@NonNull
	@Override
	public HolderCurso onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View v = LayoutInflater.from(c).inflate(R.layout.tarjeta_curso,viewGroup,false);
		
		
		return new HolderCurso(v);	}
	
	@Override
	public void onBindViewHolder(@NonNull final HolderCurso holderCurso, final int i) {
		
		holderCurso.getTituloCurso().setText(listCursos.get(i).getNombre_curso());
		holderCurso.getDescripcionCurso().setText(listCursos.get(i).getDescripcion_curso());
		
		
		holderCurso.getTarjetaCurso().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				GRUPOS.clear();
			
				//GruposFrament gruposFrament = new GruposFrament();
				String cursoParametro= listCursos.get(i).getId_curso();
				
				LayoutInflater inflater = (LayoutInflater) c.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
				
				final Dialog builder = new Dialog(c);
				
				builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
				builder.setCancelable(false);
				
				View dialoglayout = inflater.inflate(R.layout.tarjeta_lista_grupos, null);
				builder.setContentView(dialoglayout);
				
				cerrarDialogo= dialoglayout.findViewById(R.id.cerrarDialogo);
				
				cerrarDialogo.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						builder.dismiss();
					}
				});
				
				recyclerViewGrupos = dialoglayout.findViewById(R.id.recyler_grupos_lista);
				nombre_Curso_Info=dialoglayout.findViewById(R.id.titulo_Curso_Informacion);
				nombre_Curso_Info.setText(listCursos.get(i).getNombre_curso());
				
				recyclerViewGrupos.setLayoutManager(new LinearLayoutManager(c));
				
				String url= CONSTANTES.linkBaseApi+"consultarGrupos&curso="+cursoParametro;
				//Crear un objeto requetqueue
				RequestQueue requestQueue = Volley.newRequestQueue(c);
				
				//Url del webservice
				// crear un objeto requet
				JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						
						JSONArray json = response.optJSONArray("educacion");
						
						JSONObject jsonObject = null;
						
						try {
							
							for (int i =0 ; i<json.length();i++){
								jsonObject = json.getJSONObject(i);
								String id_grupo   =  jsonObject.optString("grup_id");
								String nombre_grupo   =  jsonObject.optString("grup_nombre");
								String descripcion_grupo   =  jsonObject.optString("grup_descripcion");
								String id_curso_grupo =  jsonObject.optString("cur_id");
								GRUPOS.add(new Grupo(id_grupo, nombre_grupo, id_curso_grupo,descripcion_grupo ));
							}
							
							adapterGrupos = new AdaptadorGrupos(GRUPOS,c);
							recyclerViewGrupos.setAdapter(adapterGrupos);
							
						} catch (Exception e) {
							Toast.makeText(c, "Hay un problema de excepción: " + e, Toast.LENGTH_SHORT).show();
						}
						
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						
						Toast.makeText(c, "Hay un problema de volley", Toast.LENGTH_SHORT).show();
						
					}
				});
				
				//añadir el objeto requet al requetqueue
				requestQueue.add(jsonObjectRequest);
				
				
				
				builder.show();
				
				
				/*
				Bundle burdilito = new Bundle();
				burdilito.putString("idCurso", cursoParametro);
				
				
				try {
					gruposFrament.setArguments(burdilito);
					((FragmentActivity) c).getFragmentManager().beginTransaction().replace(R.id.frame_contenedorGrupos, gruposFrament).addToBackStack(null).commit();
					
					
					
				}catch (Exception e){
					System.out.println("Este es el error: " + e.toString());
				}
			*/
			
			
			}
		});
	}
	
	@Override
	public int getItemCount() {
		return listCursos.size();
	}
}
