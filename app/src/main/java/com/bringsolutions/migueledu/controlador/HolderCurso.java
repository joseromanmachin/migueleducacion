package com.bringsolutions.migueledu.controlador;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bringsolutions.migueledu.R;

public class HolderCurso extends RecyclerView.ViewHolder  {
	
	TextView tituloCurso, descripcionCurso;
	CardView tarjetaCurso;
	
	
	
	public HolderCurso(@NonNull View itemView) {
		super(itemView);
		
		//enalanzando elementos
		
		tituloCurso = itemView.findViewById(R.id.tarjeta_txNombreCurso);
		descripcionCurso = itemView.findViewById(R.id.tarjeta_txtDescripcionCurso);
		tarjetaCurso=itemView.findViewById(R.id.tarjetitaCursito);
	}
	
	public TextView getTituloCurso() {
		return tituloCurso;
	}
	
	public void setTituloCurso(TextView tituloCurso) {
		this.tituloCurso = tituloCurso;
	}
	
	public TextView getDescripcionCurso() {
		return descripcionCurso;
	}
	
	public void setDescripcionCurso(TextView descripcionCurso) {
		this.descripcionCurso = descripcionCurso;
	}
	
	public CardView getTarjetaCurso() {
		return tarjetaCurso;
	}
	
	public void setTarjetaCurso(CardView tarjetaCurso) {
		this.tarjetaCurso = tarjetaCurso;
	}
	
	
}
