package com.bringsolutions.migueledu.controlador;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bringsolutions.migueledu.R;
import com.bringsolutions.migueledu.modelo.Grupo;
import com.bringsolutions.migueledu.vista.InformacionGrupo;

import java.util.List;

public class AdaptadorGrupos extends RecyclerView.Adapter<AdaptadorGrupos.ViewHolder> {
	
	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		TextView tituloGrupo, descripcionGrupo;
		CardView tarjetaGrupo;
		
		public ViewHolder(View itemView) {
			super(itemView);
			//enalanzando elementos
			
			tituloGrupo = itemView.findViewById(R.id.tarjeta_txtNombreGrupo);
			descripcionGrupo = itemView.findViewById(R.id.tarjeta_txtDescripcionGrupo);
			tarjetaGrupo=itemView.findViewById(R.id.tarjetitaGrupito);
			
		}
	}
		public List<Grupo> gruposLista;
	public Context context;
	
	public AdaptadorGrupos(List<Grupo> gruposList, Context context) {
		this.gruposLista = gruposList;
		this.context = context;
	}
	
	@NonNull
	@Override
	public AdaptadorGrupos.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_grupo,viewGroup,false);
		return new AdaptadorGrupos.ViewHolder(view);
	}
	
	
	@Override
	public void onBindViewHolder(@NonNull final AdaptadorGrupos.ViewHolder viewHolder, final  int i) {
		viewHolder.tituloGrupo.setText(gruposLista.get(i).getNombre_grupo());
		viewHolder.descripcionGrupo.setText(gruposLista.get(i).getGrupo_descripcion());
		viewHolder.tarjetaGrupo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				String idGrupo_Valor=gruposLista.get(i).getId_grupo();
				String nombreGrupo_Valor= gruposLista.get(i).getNombre_grupo();
				String descripcionGrupo_Valor=gruposLista.get(i).getGrupo_descripcion();
				
				
				Intent i=new Intent(context, InformacionGrupo.class);
				i.putExtra("idGrupito", idGrupo_Valor);
				i.putExtra("nombreGrupito", nombreGrupo_Valor);
				i.putExtra("descripcionGrupito",descripcionGrupo_Valor);
				context.startActivity(i);
			}
		});
	}
	
	@Override
	public int getItemCount() {
		return gruposLista.size();
	}
}
