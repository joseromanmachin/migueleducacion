package com.bringsolutions.migueledu.controlador;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bringsolutions.migueledu.R;
import com.bringsolutions.migueledu.modelo.Alumno;
import com.bringsolutions.migueledu.vista.InformacionGrupo;

import java.util.List;

public class AdaptadorAlumnos extends RecyclerView.Adapter<AdaptadorAlumnos.ViewHolder> {
	
	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		TextView nombreAlumno, apPaternoAlumno, apMaternoAlumno, emailAlumno, telefonoAlumno;
		
		CardView tarjetaAlumno;
		
		public ViewHolder(View itemView) {
			super(itemView);
			//enalanzando elementos
			
			nombreAlumno = itemView.findViewById(R.id.tarjeta_txtNombreAlumno);
			apPaternoAlumno = itemView.findViewById(R.id.tarjeta_txtApellidoPaternoAlumno);
			apMaternoAlumno = itemView.findViewById(R.id.tarjeta_txtApellidoMaternoAlumno);
			emailAlumno = itemView.findViewById(R.id.tarjeta_txtEmailAlumno);
			telefonoAlumno = itemView.findViewById(R.id.tarjeta_txtTelefonoAlumno);
			tarjetaAlumno=itemView.findViewById(R.id.tarjetitaAlumno);
			
		}
	}
	public List<Alumno> alumnosLista;
	public Context context;
	
	public AdaptadorAlumnos(List<Alumno> alumnosList, Context context) {
		this.alumnosLista = alumnosList;
		this.context = context;
	}
	
	
	@NonNull
	@Override
	public AdaptadorAlumnos.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_alumno,viewGroup,false);
		return new AdaptadorAlumnos.ViewHolder(view);
	}
	
	@Override
	public void onBindViewHolder(@NonNull final AdaptadorAlumnos.ViewHolder viewHolder, final  int i) {
		viewHolder.nombreAlumno.setText(alumnosLista.get(i).getNombre_Alumno());
		viewHolder.apPaternoAlumno.setText(alumnosLista.get(i).getApPaterno_Alumno());
		viewHolder.apMaternoAlumno.setText(alumnosLista.get(i).getApMaterno_Alumno());
		viewHolder.emailAlumno.setText(alumnosLista.get(i).getEmail_Alumno());
		viewHolder.telefonoAlumno.setText(alumnosLista.get(i).getTelefono_Alumno());
		
		viewHolder.tarjetaAlumno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Toast.makeText(context, "Alumno: "+ alumnosLista.get(i).getNombre_Alumno(), Toast.LENGTH_LONG).show();
			}
		});
		
	}
	
	
	@Override
	public int getItemCount() {
		return alumnosLista.size();
	}
}
